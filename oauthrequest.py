#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import string   # uses ascii_letters to build nonce
import time     # uses time() to get the current timestamp
import urllib
import urllib2
import urlparse
import hmac
import base64
import random

class OAuthRequest(urllib2.Request):
    """A request signed with OAuth."""

    def __init__(self, key, secret, params=None, token=None, *args, **kwargs):
        """Starts an OAuth-signed request. Accepts all normal urllib2.Request
        parameters, plus::

        key
            The Consumer Key you received when you registered your
            application.

        secret
            The Consumer Secret you received when you registered your
            application.

        params
            Parameters you want to pass in a GET request. OAuthRequest follows
            the same methodology of urllib2.Request: If you don't use
            set_data(), it will be a GET request; if you use set_data(),
            though, the request will use POST.

        token
            The Access Token you have. If you are requesting the token itself,
            you don't need to add this parameter.
        """
        self._log = logging.getLogger('OAuthRequest')
        self._secret = secret
        self._key = key
        if token:
            self._token = token
        else:
            self._token = ''

        self._params = {
            'oauth_timestamp': '',      # will be set on sign()
            'oauth_nonce': '',          # will be set on sign()
            'oauth_version': '1.0',
            'oauth_consumer_key': key,
            'oauth_signature_method': 'HMAC-SHA1'
        }
        if params:
            self._params.update(params)

        urllib2.Request.__init__(self, *args, **kwargs)
        return

    def _nonce(self):
        """Build a nonce value."""
        nonce = []
        while len(nonce) < 12:
            nonce.append(random.choice(string.ascii_letters))
        nonce = ''.join(nonce)
        self._log.debug('Nonce: ', nonce)
        return nonce

    def _timestamp(self):
        """Build a timestamp."""
        now = str(int(time.time()))
        self._log.debug('Timestamp: ', now)
        return now

    def set_param(self, key, value):
        """Set a parameter."""
        self._params[key] = value
        self._log.debug('Updated %s = %s', key, value)
        return

    def _normalize_url(self):
        """Normalizes the URL."""
        url = self.get_host()
        if ':' in url:
            # The the port in URL is standard, removed it.
            url_frags = url.split(':')
            if((self.get_type() == 'http' and url_frags[1] == '80') or
                    (self.get_type() == 'https' and url_frags[1] == '443') or
                    (self.get_type() == 'ftp' and url-frags[1] == 20)):
                url = url_frags[0]

        # we also need the path, but not the parameters (if any -- there
        # shouldn't be any, everything should go in the params or in the body
        # but, just in case...)
        selector = self.get_selector()
        if '?' in selector:
            # yup, parameters there.
            selector_frags = selector.split('?')
            selector = selector_frags[0]

        normalized = self.get_type() + '://' + url + selector
        normalized = urllib.quote(normalized)
        self._log.debug('Normalized URL: %s', normalized)
        return normalized

    def _ordered_params(self):
        """Build a request with the parameters in order."""
        params = []
        for key in sorted(self._params.keys()):
            params.append(key + '=' + urllib.quote(self._params[key]))
        ordered = '&'.join(params)
        self._log.debug('Ordered params: %s', ordered)
        return ordered

    def _elements(self):
        """Build a string with all requests elements, including method and
        URL. This is used in the signature."""
        sign = []
        sign.append(self.get_method().upper())
        sign.append(self._normalize_url())
        sign.append(self._ordered_params())
        # TODO: Use the get_data() (body) too

        elements = '&'.join(sign)
        self._log.debug('Elements: %s', elements)
        return elements

    def sign(self):
        """Sign the request. Since all signatures use a timestamp and servers
        will complain if the difference between the request timestamp and
        their current time, you should sign as close as possible to
        urlopen()."""
        self._params['oauth_timestamp'] = self._timestamp()
        self._params['oauth_nonce'] = self._nonce()

        sign_base = self._elements()
        sign_key = self._key + '&' + self._token
        self._log.debug('HMAC key: %s', sign_key)
        signature = base64.b64encode(hmac.new(sign_key, sign_base).digest())
        self._log.debug('Signature: %s', signature)

        self._params['oauth_signature'] = signature
        self._params['oauth_signature_method'] = 'HMAC-SHA1'

        # oauth params also become headers
        # (Just for sure, add them ordered too)
        for key in sorted(self._params.keys()):
            if key.startswith('oauth') or key.startswith('xoauth'):
                self._log.debug('Adding %s as header', key)
                self.add_header(key, self._params[key])

        query = self._ordered_params()

        # redo the URL with the parameters
        # We can "trick" Request by replacing "host" with our new request,
        # since get_host() can look at it (or some __r_host variable that
        # is built when your pass an URL) and that's the value
        # AbstractHTTPHandler uses to open an URL.
        # We need to add a port if it isn't there yet.s
        #url = self.get_host()
        #if not ':' in url:
        #    if self.get_type() == 'http':
        #        url += ':80'
        ##    elif self.get_type() == 'https':
        #       url += ':443'
        #    elif self.get_type() == 'ftp':
        #        url += ':20'
        #    else:
        #        raise RuntimeError('Unknown protocol')
        host = (self.get_type() + '://' + self.get_host() +
                self.get_selector() + '?' + query)
        (self.type, _r_type) = urllib.splittype(host)
        (self.host, _r_host) = urllib.splithost(_r_type)
        self._log.debug('Request: %s', self.host)
        return